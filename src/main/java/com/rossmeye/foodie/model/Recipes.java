package com.rossmeye.foodie.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Recipes {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int entry_num;

    private String name;

    private LocalDate time;

    private String recipe_link;

    private int user_id;

//    public Recipes(String name, double time, String recipe_link, int user_id){
//        this.name=name;
//        this.time = time;
//        this.recipe_link = recipe_link;
//        this.user_id = user_id;
//    }

    public String getName(){
        return name;
    }

    public LocalDate getTime(){
        return time;
    }

    public String getRecipe(){
        return recipe_link;
    }

    public int getUserId(){
        return user_id;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setTime(LocalDate time){
        this.time = time;
    }

    public void setRecipeLink(String recipe_link){
        this.recipe_link = recipe_link;
    }

    public void setUserId(int user_id){
        this.user_id = user_id;
    }

}
