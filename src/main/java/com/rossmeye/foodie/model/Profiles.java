package com.rossmeye.foodie.model;

import javax.persistence.*;

@Entity
public class Profiles {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int user_id;

    private String first;

    private String last;

    private String email;

    private String password;

//    public Profiles(String first, String last, String email, String password){
//        this.first = first;
//        this.last = last;
//        this.email = email;
//        this.password = password;
//    }


    public String getfirst(){
        return first;
    }

    public String getLast() {
        return last;
    }

    public String getEmail(){
        return email;
    }

    public String getPass(){
        return password;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setLast(String last){
        this.last = last;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setPassword(String password){
        this.password = password;
    }

}
