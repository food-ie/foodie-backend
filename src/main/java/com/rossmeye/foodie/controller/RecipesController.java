package com.rossmeye.foodie.controller;

import com.rossmeye.foodie.model.Recipes;
import com.rossmeye.foodie.repository.RecipesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/recipes")
public class RecipesController {

    @Autowired
    private RecipesRepository recipesRepository;

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Recipes> getAllRecipes(){
        return recipesRepository.findAll();
    }

    @GetMapping("/{name}")
    public void findById(@PathVariable String name){
        List<Recipes> recipes = recipesRepository.findByName(name);
    }

    @PostMapping(path="/add")
    public @ResponseBody String addRecipe(@RequestParam String name,
              @RequestParam  String recipe_url){


        LocalDate date = LocalDate.now();

        Recipes n = new Recipes();
        n.setName(name);
        n.setTime(date);
        n.setRecipeLink(recipe_url);
        recipesRepository.save(n);
        return "New Recipe Saved";


    }

}
