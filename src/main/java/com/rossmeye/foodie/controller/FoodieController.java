package com.rossmeye.foodie.controller;

import com.rossmeye.foodie.model.Profiles;
import com.rossmeye.foodie.repository.ProfilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path="/profiles")
public class FoodieController {
    @Autowired
    private ProfilesRepository profilesRepository;

    @PostMapping(path="/new")
    public @ResponseBody String addNewProfile (@RequestParam String first,
        @RequestParam String last, @RequestParam String email, @RequestParam String password) {

        Profiles n = new Profiles();
        n.setFirst(first);
        n.setLast(last);
        n.setEmail(email);
        n.setPassword(password);
        profilesRepository.save(n);
        return "New User Created";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Profiles> getAllProfiles(){
        return profilesRepository.findAll();
    }

}
