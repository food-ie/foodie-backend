package com.rossmeye.foodie.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.rossmeye.foodie.model.Profiles;

public interface ProfilesRepository extends CrudRepository<Profiles, Integer>{
}
