package com.rossmeye.foodie.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.rossmeye.foodie.model.Recipes;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RecipesRepository extends CrudRepository<Recipes, Integer> {

//    @Query("SELECT r.name FROM Recipes r where r.entry_num = :entry_num")
//    List<Recipes> findByRecipeId(@Param("entry_num") Integer entry_num);

    List<Recipes> findByName(String name);
}
